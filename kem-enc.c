/* kem-enc.c
 * simple encryption utility providing CCA2 security.
 * based on the KEM/DEM hybrid model. */

#include <fcntl.h>
#include <getopt.h>
#include <openssl/sha.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "prf.h"
#include "rsa.h"
#include "ske.h"

static const char* usage =
    "Usage: %s [OPTIONS]...\n"
    "Encrypt or decrypt data.\n\n"
    "   -i,--in     FILE   read input from FILE.\n"
    "   -o,--out    FILE   write output to FILE.\n"
    "   -k,--key    FILE   the key.\n"
    "   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
    "   -e,--enc           encrypt (this is the default action).\n"
    "   -d,--dec           decrypt.\n"
    "   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
    "   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
    "                      RSA key; the symmetric key will always be 256 "
    "bits).\n"
    "                      Defaults to %lu.\n"
    "   --help             show this message and exit.\n";

#define FNLEN 255

enum modes { ENC, DEC, GEN };

/* Let SK denote the symmetric key.  Then to format ciphertext, we
 * simply concatenate:
 * +------------+----------------+
 * | RSA-KEM(X) | SKE ciphertext |
 * +------------+----------------+
 * NOTE: reading such a file is only useful if you have the key,
 * and from the key you can infer the length of the RSA ciphertext.
 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the
 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",
 * so we will use different hash functions:  H := SHA256, while
 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c
 * (see KDF_KEY).
 * */

#define HASHLEN 32       /* for sha256 */
#define ENTROPY_BYTES 32 /* for X */

int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K) {
  /* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;
   * encrypt fnIn with SK; concatenate encapsulation and cihpertext;
   * write to fnOut. */

  // Generate SK: KDF(X)
  SKE_KEY ske_key;
  unsigned char* buf = malloc(ENTROPY_BYTES); /* X */
  randBytes(buf, ENTROPY_BYTES);
  ske_keyGen(&ske_key, buf, ENTROPY_BYTES);

  size_t n_bytes = mpz_size(K->n) * sizeof(mp_limb_t);
  // RSA encrypt X
  unsigned char* rsa_buf = malloc(n_bytes);
  memset(rsa_buf, 0, n_bytes);
  size_t written = rsa_encrypt(rsa_buf, buf, ENTROPY_BYTES, K);

  // Compute SHA256 of X
  unsigned char* hash = malloc(HASHLEN);
  SHA256(buf, HASHLEN, hash);

  // Encrypt file
  unsigned char* IV = malloc(AES_BLOCK_SIZE);
  randBytes(IV, AES_BLOCK_SIZE);
  ske_encrypt_file(fnOut, fnIn, &ske_key, IV, n_bytes + HASHLEN);

  // Concatenate KEM encapsulation with ske_ciphertext
  size_t KEM_len = n_bytes + HASHLEN;
  unsigned char* KEM = malloc(KEM_len);
  memcpy(KEM, rsa_buf, n_bytes);         // Copy RSA(X) to KEM
  memcpy(KEM + n_bytes, hash, HASHLEN);  // Copy H(X) to KEM

  // write KEM to first 64 bytes of ske_ciphertext
  int fd = open(fnOut, O_WRONLY);
  write(fd, KEM, n_bytes + HASHLEN);
  close(fd);

  return 0;
}

/* NOTE: make sure you check the decapsulation is valid before continuing */
int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K) {
  /* TODO: write this. */
  /* step 1: recover the symmetric key */
  /* step 2: check decapsulation */
  /* step 3: derive key from ephemKey and decrypt data. */

  int fd = open(fnIn, O_RDONLY);

  size_t n_bytes = mpz_size(K->n) * sizeof(mp_limb_t);
  unsigned char* KEM = malloc(n_bytes + HASHLEN);
  read(fd, KEM, n_bytes + HASHLEN);

  // Recover x from rsa_decrypt
  unsigned char* recovered_x = malloc(ENTROPY_BYTES);

  rsa_decrypt(recovered_x, KEM, n_bytes, K);

  // check if hash(recovered_x) == hash(X)
  unsigned char* hash_of_recovered_x = malloc(HASHLEN);
  SHA256(recovered_x, HASHLEN, hash_of_recovered_x);

  unsigned char* hash_of_x = malloc(HASHLEN);
  memcpy(hash_of_x, KEM + n_bytes, HASHLEN);

  if (memcmp(hash_of_recovered_x, hash_of_x, HASHLEN) != 0) {
    return -1;
  }
  // if we're here, we can do key generation. yay!!
  SKE_KEY ske_key;
  ske_keyGen(&ske_key, recovered_x, ENTROPY_BYTES);

  close(fd);
  // time to get plaintext now that we have sk
  ske_decrypt_file(fnOut, fnIn, &ske_key, n_bytes + HASHLEN);

  return 0;
}

int main(int argc, char* argv[]) {
  /* define long options */
  static struct option long_opts[] = {
      {"in", required_argument, 0, 'i'},  {"out", required_argument, 0, 'o'},
      {"key", required_argument, 0, 'k'}, {"rand", required_argument, 0, 'r'},
      {"gen", required_argument, 0, 'g'}, {"bits", required_argument, 0, 'b'},
      {"enc", no_argument, 0, 'e'},       {"dec", no_argument, 0, 'd'},
      {"help", no_argument, 0, 'h'},      {0, 0, 0, 0}};
  /* process options: */
  char c;
  int opt_index = 0;
  char fnRnd[FNLEN + 1] = "/dev/urandom";
  fnRnd[FNLEN] = 0;
  char fnIn[FNLEN + 1];
  char fnOut[FNLEN + 1];
  char fnKey[FNLEN + 1];
  memset(fnIn, 0, FNLEN + 1);
  memset(fnOut, 0, FNLEN + 1);
  memset(fnKey, 0, FNLEN + 1);
  int mode = ENC;
  // size_t nBits = 2048;
  size_t nBits = 1024;
  while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts,
                          &opt_index)) != -1) {
    switch (c) {
      case 'h':
        printf(usage, argv[0], nBits);
        return 0;
      case 'i':
        strncpy(fnIn, optarg, FNLEN);
        break;
      case 'o':
        strncpy(fnOut, optarg, FNLEN);
        break;
      case 'k':
        strncpy(fnKey, optarg, FNLEN);
        break;
      case 'r':
        strncpy(fnRnd, optarg, FNLEN);
        break;
      case 'e':
        mode = ENC;
        break;
      case 'd':
        mode = DEC;
        break;
      case 'g':
        mode = GEN;
        strncpy(fnOut, optarg, FNLEN);
        break;
      case 'b':
        nBits = atol(optarg);
        break;
      case '?':
        printf(usage, argv[0], nBits);
        return 1;
    }
  }

  /* TODO: finish this off.  Be sure to erase sensitive data
   * like private keys when you're done with them (see the
   * rsa_shredKey function). */
  switch (mode) {
    case ENC: {
      RSA_KEY K;
      FILE* pub_file = fopen(fnKey, "r");
      rsa_readPublic(pub_file, &K);
      kem_encrypt(fnOut, fnIn, &K);
      fclose(pub_file);
    } break;
    case DEC: {
      RSA_KEY K;
      FILE* priv_file = fopen(fnKey, "r");
      rsa_readPrivate(priv_file, &K);
      kem_decrypt(fnOut, fnIn, &K);
      fclose(priv_file);
      rsa_shredKey(&K);
    } break;
    case GEN: {
      RSA_KEY K;
      rsa_keyGen(nBits, &K);
      FILE* priv_file = fopen(fnOut, "w");
      rsa_writePrivate(priv_file, &K);
      fclose(priv_file);
      FILE* pub_file = fopen(strcat(fnOut, ".pub"), "w");
      rsa_writePublic(pub_file, &K);
      fclose(pub_file);
      rsa_shredKey(&K);
    } break;
    default:
      return 1;
  }

  return 0;
}
