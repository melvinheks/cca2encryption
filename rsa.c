#include "rsa.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "prf.h"

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x, 10)
#define NEWZ(x) \
  mpz_t x;      \
  mpz_init(x)
#define BYTES2Z(x, buf, len) mpz_import(x, len, -1, 1, 0, 0, buf)
#define Z2BYTES(buf, len, x) mpz_export(buf, &len, -1, 1, 0, 0, x)

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x) {
  size_t i, len = mpz_size(x) * sizeof(mp_limb_t);
  size_t templen = len;
  unsigned char* buf = malloc(len);
  /* force little endian-ness: */
  for (i = 0; i < 8; i++) {
    unsigned char b = (len >> 8 * i) % 256;
    fwrite(&b, 1, 1, f);
  }
  memset(buf, 0, len);
  Z2BYTES(buf, templen, x);
  fwrite(buf, 1, len, f);
  /* kill copy in buffer, in case this was sensitive: */
  memset(buf, 0, len);
  free(buf);
  return 0;
}
int zFromFile(FILE* f, mpz_t x) {
  size_t i, len = 0;
  /* force little endian-ness: */
  for (i = 0; i < 8; i++) {
    unsigned char b;
    /* XXX error check this; return meaningful value. */
    fread(&b, 1, 1, f);
    len += (b << 8 * i);
  }
  unsigned char* buf = malloc(len);
  memset(buf, 0, len);
  fread(buf, 1, len, f);
  BYTES2Z(x, buf, len);
  /* kill copy in buffer, in case this was sensitive: */
  memset(buf, 0, len);
  free(buf);
  return 0;
}

/* TODO: write this.  Use the prf to get random byte strings of
 * the right length, and then test for primality (see the ISPRIME
 * macro above).  Once you've found the primes, set up the other
 * pieces of the key ({en,de}crypting exponents, and n=pq). */
int rsa_keyGen(size_t keyBits, RSA_KEY* K) {
  mpz_t t;
  mpz_init(t);
  rsa_initKey(K);
  size_t half_key_bytes = keyBits / 16;
  setSeed(NULL, half_key_bytes);
  unsigned char* buf = malloc(half_key_bytes);

  // Find primes p and q
  while (!ISPRIME(K->p)) {
    randBytes(buf, half_key_bytes);
    BYTES2Z(K->p, buf, half_key_bytes);
  }
  while (!ISPRIME(K->q) && K->p != K->q) {
    randBytes(buf, half_key_bytes);
    BYTES2Z(K->q, buf, half_key_bytes);
  }
  // n = p * q
  mpz_mul(K->n, K->p, K->q);

  // phi(n) = t = (p - 1)(q - 1)
  mpz_t t_p, t_q;
  mpz_inits(t_p, t_q, NULL);
  mpz_sub_ui(t_p, K->p, 1);
  mpz_sub_ui(t_q, K->q, 1);
  mpz_mul(t, t_p, t_q);

  // Find e such that gcd(e, t) = 1
  mpz_t gcd;
  mpz_init(gcd);
  for (mpz_set_ui(K->e, 2); mpz_cmp(K->e, t) < 0; mpz_add_ui(K->e, K->e, 1)) {
    mpz_gcd(gcd, K->e, t);
    if (mpz_cmp_ui(gcd, 1) == 0) {
      break;
    }
  }
  // Calculate d using extended gcd given e, t
  mpz_invert(K->d, K->e, t);
  mpz_clears(gcd, t, t_p, t_q, NULL);
  return 0;
}

size_t rsa_encrypt(unsigned char* outBuf,
                   unsigned char* inBuf,
                   size_t len,
                   RSA_KEY* K) {
  /* TODO: write this.  Use BYTES2Z to get integers, and then
   * Z2BYTES to write the output buffer. */
  //  c = m^e mod n
  mpz_t m, c;
  mpz_inits(m, c, NULL);
  BYTES2Z(m, inBuf, len);
  mpz_powm(c, m, K->e, K->n);
  Z2BYTES(outBuf, len, c);
  mpz_clears(m, c, NULL);
  return len; /* TODO: return should be # bytes written */
}
size_t rsa_decrypt(unsigned char* outBuf,
                   unsigned char* inBuf,
                   size_t len,
                   RSA_KEY* K) {
  /* TODO: write this.  See remarks above. */
  // m = c^d mod n
  mpz_t c, m;
  mpz_inits(c, m, NULL);
  BYTES2Z(c, inBuf, len);
  mpz_powm(m, c, K->d, K->n);
  Z2BYTES(outBuf, len, m);
  mpz_clears(c, m, NULL);
  return len;
}

size_t rsa_numBytesN(RSA_KEY* K) {
  return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K) {
  mpz_init(K->d);
  mpz_set_ui(K->d, 0);
  mpz_init(K->e);
  mpz_set_ui(K->e, 0);
  mpz_init(K->p);
  mpz_set_ui(K->p, 0);
  mpz_init(K->q);
  mpz_set_ui(K->q, 0);
  mpz_init(K->n);
  mpz_set_ui(K->n, 0);
  return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K) {
  /* only write n,e */
  zToFile(f, K->n);
  zToFile(f, K->e);
  return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K) {
  zToFile(f, K->n);
  zToFile(f, K->e);
  zToFile(f, K->p);
  zToFile(f, K->q);
  zToFile(f, K->d);
  return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K) {
  rsa_initKey(K); /* will set all unused members to 0 */
  zFromFile(f, K->n);
  zFromFile(f, K->e);
  return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K) {
  rsa_initKey(K);
  zFromFile(f, K->n);
  zFromFile(f, K->e);
  zFromFile(f, K->p);
  zFromFile(f, K->q);
  zFromFile(f, K->d);
  return 0;
}
int rsa_shredKey(RSA_KEY* K) {
  /* clear memory for key. */
  mpz_t* L[5] = {&K->d, &K->e, &K->n, &K->p, &K->q};
  size_t i;
  for (i = 0; i < 5; i++) {
    size_t nLimbs = mpz_size(*L[i]);
    if (nLimbs) {
      memset(mpz_limbs_write(*L[i], nLimbs), 0, nLimbs * sizeof(mp_limb_t));
      mpz_clear(*L[i]);
    }
  }
  /* NOTE: a quick look at the gmp source reveals that the return of
   * mpz_limbs_write is only different than the existing limbs when
   * the number requested is larger than the allocation (which is
   * of course larger than mpz_size(X)) */
  return 0;
}
