#include "ske.h"
#include <errno.h>
#include <fcntl.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include "prf.h"
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE | MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+----------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(IV|C) (32 bytes for SHA256) |
 * +------------+--------------------+----------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen) {
  /* TODO: write this.  If entropy is given, apply a KDF to it to get
   * the keys (something like HMAC-SHA512 with KDF_KEY will work).
   * If entropy is null, just get a random key (you can use the PRF). */
  unsigned char gen_key[KLEN_SKE * 2];
  if (!entropy) {
    randBytes(gen_key, KLEN_SKE * 2);
  } else {
    HMAC(EVP_sha512(), KDF_KEY, KLEN_SKE, entropy, entLen, gen_key, NULL);
  }
  memcpy(K->hmacKey, gen_key, KLEN_SKE);
  memcpy(K->aesKey, gen_key + KLEN_SKE, KLEN_SKE);
  return 0;
}
size_t ske_getOutputLen(size_t inputLen) {
  return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf,
                   unsigned char* inBuf,
                   size_t len,
                   SKE_KEY* K,
                   unsigned char* IV) {
  /* TODO: finish writing this.  Look at ctr_example() in aes-example.c
   * for a hint.  Also, be sure to setup a random IV if none was given.
   * You can assume outBuf has enough space for the result. */
  if (!IV) {
    IV = malloc(AES_BLOCK_SIZE);
    randBytes(IV, AES_BLOCK_SIZE);
  }
  memcpy(outBuf, IV, AES_BLOCK_SIZE);
  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, IV))
    ERR_print_errors_fp(stderr);
  int nWritten;
  if (1 !=
      EVP_EncryptUpdate(ctx, outBuf + AES_BLOCK_SIZE, &nWritten, inBuf, len))
    ERR_print_errors_fp(stderr);
  EVP_CIPHER_CTX_free(ctx);
  unsigned int hmWritten;
  HMAC(EVP_sha256(), K->hmacKey, KLEN_SKE, outBuf, AES_BLOCK_SIZE + nWritten,
       outBuf + AES_BLOCK_SIZE + nWritten, &hmWritten);

  return AES_BLOCK_SIZE + nWritten +
         hmWritten; /* TODO: should return number of bytes written, which
hopefully matches ske_getOutputLen(...). */
}
size_t ske_encrypt_file(const char* fnout,
                        const char* fnin,
                        SKE_KEY* K,
                        unsigned char* IV,
                        size_t offset_out) {
  /* TODO: write this.  Hint: mmap. */
  int fd = open(fnin, O_RDONLY);
  int outfd = open(fnout, O_CREAT | O_RDWR | O_TRUNC, (mode_t)0744);
  struct stat s;
  int status = fstat(fd, &s);
  size_t sizein = s.st_size;
  size_t sizeout = offset_out + sizein + AES_BLOCK_SIZE + HM_LEN;
  ftruncate(outfd, sizeout);
  unsigned char* infile = mmap(NULL, sizein, PROT_READ, MAP_PRIVATE, fd, 0);
  unsigned char* outfile =
      mmap(NULL, sizeout, PROT_READ | PROT_WRITE, MAP_SHARED, outfd, 0);
  if (outfile == MAP_FAILED) {
    perror("The error");
    return -1;
  }
  int nWritten = ske_encrypt(outfile + offset_out, infile, sizein, K, IV);
  munmap(infile, sizein);
  if (msync(outfile, sizeout, MS_SYNC)) {
    perror("Im dying");
  }
  munmap(outfile, sizeout);
  close(fd);
  close(outfd);
  return nWritten;
}
size_t ske_decrypt(unsigned char* outBuf,
                   unsigned char* inBuf,
                   size_t len,
                   SKE_KEY* K) {
  /* TODO: write this.  Make sure you check the mac before decypting!
   * Oh, and also, return -1 if the ciphertext is found invalid.
   * Otherwise, return the number of bytes written.  See aes-example.c
   * for how to do basic decryption. */
  // inBuf + AES_BLOCK_SIZE for start of ct, len-AES_BLOCK_SIZE-HM_LEN for
  // length of ct
  unsigned char mac[HM_LEN];
  HMAC(EVP_sha256(), K->hmacKey, KLEN_SKE, inBuf, len - HM_LEN, mac, NULL);
  if (memcmp(mac, inBuf + len - HM_LEN, HM_LEN) != 0) {
    return -1;
  }
  EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
  if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, inBuf))
    ERR_print_errors_fp(stderr);
  int nWritten;
  if (1 != EVP_DecryptUpdate(ctx, outBuf, &nWritten, inBuf + AES_BLOCK_SIZE,
                             len - AES_BLOCK_SIZE - HM_LEN))
    ERR_print_errors_fp(stderr);
  return nWritten;
}
size_t ske_decrypt_file(const char* fnout,
                        const char* fnin,
                        SKE_KEY* K,
                        size_t offset_in) {
  /* TODO: write this. */
  int fd = open(fnin, O_RDONLY);
  int outfd = open(fnout, O_CREAT | O_RDWR | O_TRUNC, (mode_t)0744);
  struct stat s;
  int status = fstat(fd, &s);
  size_t sizein = s.st_size;
  size_t sizeout = sizein - AES_BLOCK_SIZE - HM_LEN - offset_in;
  ftruncate(outfd, sizeout);
  unsigned char* infile = mmap(NULL, sizein, PROT_READ, MAP_PRIVATE, fd, 0);
  unsigned char* outfile =
      mmap(NULL, sizeout, PROT_READ | PROT_WRITE, MAP_SHARED, outfd, 0);
  if (outfile == MAP_FAILED) {
    perror("The error");
    return -1;
  }
  int nWritten =
      ske_decrypt(outfile, infile + offset_in, sizein - offset_in, K);
  munmap(infile, sizein);
  if (msync(outfile, sizeout, MS_SYNC)) {
    perror("Im dying");
  }
  munmap(outfile, sizeout);
  close(fd);
  close(outfd);
  return nWritten;
}
